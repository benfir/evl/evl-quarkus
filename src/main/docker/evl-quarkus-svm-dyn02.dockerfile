FROM cescoffier/native-base
WORKDIR /work/
COPY --from=benfir/evl-quarkus:0.1-svm-dyn-base /usr/src/app/target/*runner ./application
CMD ["./application", "-Dquarkus.http.host=0.0.0.0"]
