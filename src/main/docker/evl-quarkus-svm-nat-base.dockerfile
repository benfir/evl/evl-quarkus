# syntax=docker/dockerfile:experimental
FROM oracle/graalvm-ce:19.2.0

RUN gu install native-image

ENV GRAALVM_HOME="/opt/graalvm-ce-19.2.0/"

WORKDIR /usr/src/app

COPY src     ./src
COPY pom.xml ./
COPY mvnw    ./
COPY .mvn    ./.mvn

RUN --mount=type=cache,id=mvn,uid=1001,gid=1001,target=/home/root/.m2/repository ./mvnw -f /usr/src/app/pom.xml clean package -DskipTests=true -Pnative -DquarkusConf.additionalBuildArgs="--static"
