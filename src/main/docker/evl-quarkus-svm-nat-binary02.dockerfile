# syntax=docker/dockerfile:experimental
FROM fabric8/java-alpine-openjdk8-jre

COPY --from=benfir/evl-quarkus:0.1-svm-nat-base /usr/src/app/target/*runner /application
CMD ["/application", "-Dquarkus.http.host=0.0.0.0"]
