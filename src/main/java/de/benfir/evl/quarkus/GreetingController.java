package de.benfir.evl.quarkus;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path( "/hello" )
public class GreetingController
{
  @ConfigProperty( name= "greeting.message" )
  String message;
  
  @ConfigProperty( name = "greeting.suffix", defaultValue="!" )
  String suffix;
  
  // STEP1
  @GET @Produces( MediaType.TEXT_PLAIN ) public String hello()
  {
    return this.message + this.suffix;
  }
  
//  // STEP2
//  @Inject GreetingService service;
//
//  @GET @Produces( MediaType.TEXT_PLAIN )
//  @Path( "/{name}" )
//  public String greeting( @PathParam( "name" ) String name )
//  {
//    return this.service.greeting( name );
//  }
}








