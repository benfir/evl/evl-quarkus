#!/bin/bash

#set -eu

INT_NETWORK_NAME="dmo_net"
INT_CONTAINER_NAME="mysql"

#

docker network inspect $INT_NETWORK_NAME >/dev/null 2>&1;
INT_RESULT=$?

if [ $INT_RESULT -ne 0 ]; then
  docker network create dmo_net
  echo "network $INT_NETWORK_NAME created"
else
  echo "network $INT_NETWORK_NAME exists already"
fi

docker container inspect $INT_CONTAINER_NAME >/dev/null 2>&1
INT_RESULT=$?

if [ $INT_RESULT -ne 0 ]; then
  docker container run -d --rm \
    --name=mysql               \
    --user=mysql               \
    --publish 3306:3306        \
    --network dmo_net          \
    --env "MYSQL_ROOT_PASSWORD=9876" \
    --env "MYSQL_USER=test"          \
    --env "MYSQL_PASSWORD=1234"      \
    --env "MYSQL_DATABASE=testdb"    \
    mysql:5.7.16
  echo "container $INT_CONTAINER_NAME created"
else
  echo "container $INT_CONTAINER_NAME exists already"
fi

