FROM findepi/graalvm:19.2.0
#FROM oracle/graalvm-ce:19.2.0

ENV JAVA_OPTIONS="-Dquarkus.http.host=0.0.0.0 -Djava.util.logging.manager=org.jboss.logmanager.LogManager"
ENV AB_ENABLED=jmx_exporter

COPY target/lib/* /deployments/lib/
COPY target/*-runner.jar /deployments/app.jar

WORKDIR /deployments

ENTRYPOINT [ "java", "-jar", "app.jar" ]
