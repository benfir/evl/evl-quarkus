# syntax=docker/dockerfile:1-experimental
FROM oracle/graalvm-ce:19.2.1 as build

RUN gu install native-image

WORKDIR /usr/src/app

COPY mvnw    ./
COPY .mvn    ./.mvn
COPY pom.xml ./
COPY src     ./src

RUN --mount=type=cache,sharing=locked,uid=1001,gid=1001,target=/home/root/.m2/repository ./mvnw -f /usr/src/app/pom.xml clean package -DskipTests=true -Pnative

FROM cescoffier/native-base
COPY --from=build /usr/src/app/target/*runner ./application
CMD ["./application", "-Dquarkus.http.host=0.0.0.0"]
