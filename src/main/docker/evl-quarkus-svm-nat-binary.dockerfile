# syntax=docker/dockerfile:experimental
FROM ubuntu:latest
#FROM fabric8/java-alpine-openjdk8-jre
#FROM scratch

COPY --from=benfir/evl-quarkus:0.1-svm-nat-base /usr/src/app/target/evl-quarkus-runner /application
CMD ["/application", "-Dquarkus.http.host=0.0.0.0"]
