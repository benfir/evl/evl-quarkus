# syntax=docker/dockerfile:experimental
FROM quay.io/quarkus/centos-quarkus-maven:19.2.1
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
USER root
RUN chown -R quarkus /usr/src/app
USER quarkus

# RUN mvn -f /usr/src/app/pom.xml -Pnative clean package
RUN --mount=type=cache,id=mvn,uid=1001,gid=1001,target=/home/quarkus/.m2/repository mvn -f /usr/src/app/pom.xml -DskipTests=true -Pnative clean package
