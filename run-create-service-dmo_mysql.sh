#!/bin/bash

set -eu

# # create network "dmo_net" before
# docker network create --driver overlay --attachable dmo_net

# # create docker secret
# docker secret create mysql_root_password_v1 - << EOF
# 9876
# EOF

# # create docker secret
# docker secret create mysql_user_test_password_v1 - << EOF
# 1234
# EOF

docker service create   \
  --detach=false        \
  --name=mysql          \
  --replicas=1          \
  --user=mysql          \
  --publish 3306:3306   \
  --network dmo_net     \
  --env "MYSQL_ROOT_PASSWORD_FILE=/run/secrets/mysql_root_password" \
  --env "MYSQL_USER=test"          \
  --env "MYSQL_PASSWORD_FILE=/run/secrets/mysql_user_test_password" \
  --env "MYSQL_DATABASE=testdb"    \
  --secret source=mysql_root_password_v1,target=mysql_root_password \
  --secret source=mysql_user_test_password_v1,target=mysql_user_test_password \
  mysql:5.7.16
