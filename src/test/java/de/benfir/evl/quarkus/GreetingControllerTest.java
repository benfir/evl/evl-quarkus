package de.benfir.evl.quarkus;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class GreetingControllerTest
{

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/hello")
          .then()
             .statusCode(200)
             .body(is("hello again!"));
    }
    
    @Test
    public void testGreetingEndpoint()
    {
        given()
          .pathParam( "name", "quarkus" )
          .when().get( "/hello/{name}" )
          .then()
            .statusCode( 200 )
            .body( is( "hello quarkus \n" ) );
    }

}
