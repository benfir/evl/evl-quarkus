package de.benfir.evl.quarkus;

import static org.assertj.core.api.Assertions.*;
import static io.restassured.RestAssured.*;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

@QuarkusTest
@Disabled
class TodoControllerTest
{
  @Test()
  public void testGetTodos()
  {
    Response rsp = given()
      .when()
        .get( "/todos" )
      ;
    
    assertThat( rsp.getStatusCode() ).isEqualTo( 200 );
    
    ResponseBody body = rsp.getBody();
    
    assertThat( body ).isNotNull();
    
    body.prettyPrint();
    
    List list = body.as( List.class );
    
    assertThat( list ).isNotNull();
    assertThat( list ).isNotEmpty();
    assertThat( list.size() ).isEqualTo( 1 );
  }
}
