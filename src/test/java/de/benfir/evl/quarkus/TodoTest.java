package de.benfir.evl.quarkus;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.*;

@QuarkusTest
class TodoTest
{
  @Inject TodoRepository todoRepository;
  
  @Test
  public void testGetTodos()
  {
    assertThat( this.todoRepository ).isNotNull();
    // TODO
    // see: https://github.com/quarkusio/quarkus/issues/1724
    org.junit.jupiter.api.Assertions.assertThrows( IllegalStateException.class, () -> {
      this.todoRepository.findAll();
    });
    // assertThat( this.todoRepository.findAll().list() ).isEmpty();
  }
}
