package de.benfir.evl.quarkus;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import io.quarkus.runtime.configuration.ProfileManager;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

@ApplicationScoped
public class AppLifeCycleBean
{
  final Logger log = LoggerFactory.getLogger( this.getClass().getName() );
  
  @ConfigProperty( name= "quarkus.datasource.url"    ) String datasourceUrl;
  @ConfigProperty( name= "quarkus.datasource.driver" ) String datasourceDriver;
  
  void onStart( @Observes StartupEvent event ){
  
    String profile = ProfileManager.getActiveProfile();
    
    this.log.info( "Starting ..." );
    this.log.info( ".. quarkus.profile          : " + profile );
    this.log.info( ".. quarkus.datasource.url   : " + this.datasourceUrl );
    this.log.info( ".. quarkus.datasource.driver: " + this.datasourceDriver );
  }
  
  void onStop( @Observes ShutdownEvent event ){
    this.log.info( "Stopping ..." );
  }
}
