#!/bin/bash

export GRAALVM_VERSION=19.2.1
export GRAALVM_HOME=/Library/Java/JavaVirtualMachines/graalvm-ce-${GRAALVM_VERSION}/Contents/Home
export JAVA_HOME=${GRAALVM_HOME}
